package com.activiti.extension.bean;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


public class InvoiceDetailsRowMapper implements RowMapper{

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        InvoiceDetails invDet = new InvoiceDetails();
        invDet.setInvoiceNo(rs.getString("invoice_no"));
        invDet.setCostCentre(rs.getString("costcenter"));
        invDet.setCurrency(rs.getString("curr"));
        invDet.setExternalEPEId(rs.getString("ExternalEPEID"));
        invDet.setFlex1(rs.getString("flex1"));
        invDet.setFlex2(rs.getString("flex2"));
        invDet.setFlex3(rs.getString("flex3"));
        invDet.setGlCode(rs.getString("glcode"));
        invDet.setInternalDocId(rs.getString("internal_doc_id"));
        invDet.setLineNo(rs.getString("line_no"));
        invDet.setMaterial(rs.getString("material"));
        invDet.setMaterialDesc(rs.getString("material_desc"));
        invDet.setQty(rs.getInt("qty"));
        invDet.setRate(rs.getBigDecimal("rate"));
        invDet.setTax(rs.getBigDecimal("tax"));
        invDet.setTaxType(rs.getString("tax_type"));
        return invDet;

}

}
