package com.activiti.extension.bean;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InvoiceDetails {
	@JsonProperty("invoice_no")
	private String invoiceNo;
	
	@JsonProperty("internal_doc_id")
	private String internalDocId;
	
	@JsonProperty("ExternalEPEID")
	private String externalEPEId;
	
	@JsonProperty("line_no")
	private String lineNo;
	
	@JsonProperty("material")
	private String material;
	
	@JsonProperty("material_desc")
	private String materialDesc;
	
	@JsonProperty("qty")
	private Integer qty;
	
	@JsonProperty("rate")
	private BigDecimal rate;
	
	@JsonProperty("curr")
	private String currency;
	
	@JsonProperty("glcode")
	private String glCode;
	
	@JsonProperty("costcenter")
	private String costCentre;
	
	@JsonProperty("tax")
	private BigDecimal tax;
	
	@JsonProperty("tax_type")
	private String taxType;
	
	@JsonProperty("flex1")
	private String flex1;
	
	@JsonProperty("flex2")
	private String flex2;
	
	@JsonProperty("flex3")
	private String flex3;
	
	
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public String getInternalDocId() {
		return internalDocId;
	}
	public void setInternalDocId(String internalDocId) {
		this.internalDocId = internalDocId;
	}
	public String getExternalEPEId() {
		return externalEPEId;
	}
	public void setExternalEPEId(String externalEPEId) {
		this.externalEPEId = externalEPEId;
	}
	public String getLineNo() {
		return lineNo;
	}
	public void setLineNo(String lineNo) {
		this.lineNo = lineNo;
	}
	public String getMaterial() {
		return material;
	}
	public void setMaterial(String material) {
		this.material = material;
	}
	public String getMaterialDesc() {
		return materialDesc;
	}
	public void setMaterialDesc(String materialDesc) {
		this.materialDesc = materialDesc;
	}
	public Integer getQty() {
		return qty;
	}
	public void setQty(Integer qty) {
		this.qty = qty;
	}
	public BigDecimal getRate() {
		return rate;
	}
	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getGlCode() {
		return glCode;
	}
	public void setGlCode(String glCode) {
		this.glCode = glCode;
	}
	public String getCostCentre() {
		return costCentre;
	}
	public void setCostCentre(String costCentre) {
		this.costCentre = costCentre;
	}
	public BigDecimal getTax() {
		return tax;
	}
	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}
	public String getTaxType() {
		return taxType;
	}
	public void setTaxType(String taxType) {
		this.taxType = taxType;
	}
	public String getFlex1() {
		return flex1;
	}
	public void setFlex1(String flex1) {
		this.flex1 = flex1;
	}
	public String getFlex2() {
		return flex2;
	}
	public void setFlex2(String flex2) {
		this.flex2 = flex2;
	}
	public String getFlex3() {
		return flex3;
	}
	public void setFlex3(String flex3) {
		this.flex3 = flex3;
	}
	
}
