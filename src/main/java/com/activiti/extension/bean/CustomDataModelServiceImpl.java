/**
 * Copyright (C) 2016 Alfresco Software Limited.
 * <p/>
 * This file is part of the Alfresco SDK Samples project.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.activiti.extension.bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Service;

import com.activiti.api.datamodel.AlfrescoCustomDataModelService;
import com.activiti.model.editor.datamodel.DataModelDefinitionRepresentation;
import com.activiti.model.editor.datamodel.DataModelEntityRepresentation;
import com.activiti.runtime.activiti.bean.datamodel.AttributeMappingWrapper;
import com.activiti.variable.VariableEntityWrapper;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 *
 * @author martin.bergljung@alfresco.com
 */
@Service("activiti_alfrescoCustomDataModelService")
public class CustomDataModelServiceImpl implements AlfrescoCustomDataModelService {
    private static Logger logger = LogManager.getLogger(CustomDataModelServiceImpl.class);
    /**
     * Database table names
     */
    private static final String INVOICE_DETAILS_TABLE_NAME = "invoice_details";

    /**
     * Mapping entity into JSON
     */
     
    @Autowired
    protected ObjectMapper objectMapper;

    /**
     * Use Spring JDBC Template for database access
     */
    private DataSource dataSource;
    
    private JdbcTemplate jdbcTemplate;
	
		
    public CustomDataModelServiceImpl() {
    	try
    	{
    		
    	DriverManagerDataSource ds = new DriverManagerDataSource();
        ds.setDriverClassName("com.mysql.jdbc.Driver");
        ds.setUrl("jdbc:mysql://192.168.0.148:3306/ap_poc");
        ds.setUsername("root");
        ds.setPassword("muraai");
        setDataSource(ds);
        logger.info(ds.toString());
        jdbcTemplate = new JdbcTemplate(getDataSource());

    	}
    	catch(Exception e){
    		logger.error(e.getMessage(),e);
    	}
    }

    /**
     * This method is called when Activiti wants to fetch a row from the database table
     * that has been mapped as a "Custom" entity.
     *
     * @param entityDefinition the definition of the "custom" entity that was mapped in the Custom Data Model (e.g. Salary)
     * @param fieldName the entity field that represents the PK (e.g. Employee No)
     * @param fieldValue the entity field value (e.g. 10001)
     * @return an object representing the fetched data
     */
   
    	@Override
	public String storeEntity(List<AttributeMappingWrapper> attributeDefinitionsAndValues, DataModelEntityRepresentation entityDefinition,
			DataModelDefinitionRepresentation dataModel) {
		logger.info("storeEntity() EntityDefinition [Name=" + entityDefinition.getName() +
	              "][TableName=" + INVOICE_DETAILS_TABLE_NAME + "][Id=" + entityDefinition.getId() +
	              "][Attributes=" + entityDefinition.getAttributes().size() +
	              "] [dataModel=" + dataModel.getName() +
	              "] [attributeDefinitionsAndValues=" + attributeDefinitionsAndValues.size() + "]");

	      if (StringUtils.equals(entityDefinition.getName(), "InvoiceDetails")) {
	          // Set up a map of all the column names and values
	          Map<String, Object> parameters = new HashMap<String, Object>();
	          for (AttributeMappingWrapper attributeMappingWrapper : attributeDefinitionsAndValues) {
	              // Get the column name = mapped name
	              // And the column value = attr value
	              parameters.put(attributeMappingWrapper.getAttribute().getMappedName(),
	                      attributeMappingWrapper.getValue());
	          }
	          /*String sql = "UPDATE " + SALARIES_TABLE_NAME +
	                  " SET to_date = '" +  sdf.format(parameters.get("from_date")) + "'" +
	                  " WHERE emp_no = " + parameters.get("emp_no") +
	                  " AND to_date = '9999-01-01'";
	          */
	          String sql = "UPDATE" + INVOICE_DETAILS_TABLE_NAME+ "SET internal_doc_id =" 
	          +parameters.get("internal_doc_id")+",material="+parameters.get("material")+
	        		  "WHERE invoice_no =" + parameters.get("invoice_no");
	        		  logger.info(sql);
	        		  //jdbcTemplate.update(sql);

	          
	      }
		return null;

	 
	}

	@Override
	public ObjectNode getMappedValue(DataModelEntityRepresentation entityDefinition, String fieldName, Object fieldValue) {
		try{
		logger.info("getMappedValue() EntityDefinition [Name=" + entityDefinition.getName() +
                "][TableName=" + entityDefinition.getTableName() + "][Id=" + entityDefinition.getId() +
                "][Attributes=" + entityDefinition.getAttributes().size() +
                "] [fieldName=" + fieldName +
                "] [variableValue=" + fieldValue + "]");
		
		
		
        // Check if are to get something from the INVOICEDETAILS table
        if (StringUtils.equals(entityDefinition.getName(), "InvoiceDetails")) {
            // Fetch the Invoice Details row we are looking for
            String invoiceNo = (String) fieldValue;
            String sql = "SELECT * FROM " + INVOICE_DETAILS_TABLE_NAME +
                    " WHERE invoice_no = ?;";
            
            List<InvoiceDetails> invDetList  = jdbcTemplate.query(sql,new Object[]{invoiceNo},new BeanPropertyRowMapper(InvoiceDetails.class));
            
            if(invDetList !=null && !invDetList.isEmpty())
            {
            ObjectNode invJson = objectMapper.createObjectNode();
             
            objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
            invJson.put("response_data", objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(invDetList)
            		.replaceAll("\\\"","").replaceAll("\\r", "").replaceAll("\\n", ""));            
             
            logger.info("Result is "+invJson);
            
            return invJson;
            }
            else
            	return null;
        }
        else
        	return null;
		}
		catch(Exception e){
			logger.error(e.getMessage(),e);
			e.printStackTrace();
			return null;
		}
		
	}

    @Override
    public VariableEntityWrapper getVariableEntity(String keyValue, String variableName,
                                                   String processDefinitionId,
                                                   DataModelEntityRepresentation entityValue) 
    {
try{
        logger.info("getVariableEntity() Entity [Name=" + entityValue.getName() +
                "][TableName=" + INVOICE_DETAILS_TABLE_NAME + "][Id=" + entityValue.getId() +
                "][Attributes=" + entityValue.getAttributes().size() +
                "] [keyValue=" + keyValue +
                "] [variableName=" + variableName + "]");


        return null;
    }
catch(Exception e){
	logger.error(e.getMessage(),e);
	e.printStackTrace();
	return null;
}
    }

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
   
}
