package com.activiti.extension.api;

import org.activiti.engine.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.activiti.domain.idm.User;
import com.activiti.extension.rest.Student;
import com.activiti.security.SecurityUtils;

@RestController
@RequestMapping("/enterprise/my-api-endpoint")
public class MyApiEndpoint {
	
	@Autowired
    private TaskService taskService;
	
	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public Student executeLogic(){
		
		User currentUser = SecurityUtils.getCurrentUserObject();
        long taskCount = taskService.createTaskQuery().taskAssignee(String.valueOf(currentUser.getId())).count();

		Student student = new Student();
		student.setStudentName(currentUser.getFullName());
		student.setTaskCount(taskCount);
		
		return student;
		
	}
	

}
